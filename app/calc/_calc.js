angular.module('calc', [
  'forms',
  'ui.router'
])
  .service('gameSelected', function () {
    return {
      cash: true,
      hustt: false,
      custom: false
    };
  })
  .constant('PLO_STDDEV', 140)
  .constant('NLH_STDDEV', 100)
  .constant('FR_HU_ADJUST', 15)
  .constant('HU_REG_RAKE', 5)
  .constant('HU_TURBO_RAKE', 4)
  .constant('HU_HYPER_RAKE', 2)
  ;
