angular.module('calc')
 .directive('chart', function () {
  return {
    template: '<div class="col-xs-12 col-md-7" id="chart"></div>',
    controller: 'ChartCtrl'
  };
 });
