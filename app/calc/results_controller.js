angular.module('calc')
  .controller('ResultsCtrl', function ($log, $scope, ResultsService, gameSelected) {
    var vm = this;

    vm.show = true;
    vm.winrateWord = 'winrate';
    vm.winrateSymbol = 'bb/100';

    $scope.$watch(function () { return gameSelected.hustt; }, function (isHeadsUp) {
      if (isHeadsUp) {
        vm.winrateWord = 'ROI';
        vm.winrateSymbol = '%';
      } else {
        vm.winrateWord = 'winrate';
        vm.winrateSymbol = 'bb/100';
      }
    });
    $scope.$watchCollection(function () { return ResultsService;}, function (results) {
      $log.debug('watch ResultsService', results);
      if (!results.hasOwnProperty('bayesian')) {
        vm.show = false;
        return;
      }
      vm.winrate = results.bayesian.getMean();
      vm.error = results.bayesian.getStdDev();
      vm.percentChance = results.bayesian.getPxLessThan(0) * 100;
      vm.probably1 = results.bayesian.getPercentile(0.15);
      vm.probably2 = results.bayesian.getPercentile(0.85);
      vm.definitely1 = results.bayesian.getPercentile(0.025);
      vm.definitely2 = results.bayesian.getPercentile(0.975);

      var rbMean = results.rakeback.getMean(), rbStdDev = results.rakeback.getStdDev();
      vm.rb = {
        winrate: rbMean,
        error: rbStdDev,
        percentChance: results.rakeback.getPxLessThan(0) * 100,
        probably1: rbMean - rbStdDev,
        probably2: rbMean + rbStdDev,
        definitely1: rbMean - 2 * rbStdDev,
        definitely2: rbMean + 2 * rbStdDev
      };
      vm.show = true;
    });

  });
