angular.module('calc')
  .service('ResultsService', function (
    $log, $rootScope, gameSelected, CashData, CustomData, HuSngData, PLO_STDDEV, NLH_STDDEV, FR_HU_ADJUST, HU_REG_RAKE, HU_TURBO_RAKE, HU_HYPER_RAKE
  ) {

    var results = this;

    $rootScope.$watchCollection(function () {return gameSelected;}, function (selected) {
      $log.debug('game changed', selected);
      if (selected.cash && !selected.custom && !selected.hustt) {
        updateCash(CashData);
      } else if (selected.hustt && !selected.custom && !selected.cash) {
        updateHU(HuSngData);
      } else if (selected.custom && !selected.cash && !selected.hustt) {
        updateCustom(CustomData);
      }
    });

    $rootScope.$watchCollection(function () { return CashData;}, updateCash);
    $rootScope.$watchCollection(function () { return HuSngData;}, updateHU);
    $rootScope.$watchCollection(function () { return CustomData;}, updateCustom);

    return results;


    function updateCash(data) {

      if (
        !data.hasOwnProperty('bigblind') ||
        !data.hasOwnProperty('size') ||
        !data.hasOwnProperty('game') ||
        !data.hasOwnProperty('numHands') ||
        !data.hasOwnProperty('winrate')
      ) return;

      $log.debug('updateCash', data);

      var stdDev,
        nTrials = data.numHands * 0.01,
        bbWinnings = data.winrate * nTrials / data.bigblind,
        mean = bbWinnings / nTrials,
        rake = getRake(data.game, data.bigblind);

      results.prior = new NormalDist(-rake, rake);

      if (data.game === 'PLO') {
        stdDev = PLO_STDDEV;
      } else {
        stdDev = NLH_STDDEV;
      }
      if (data.size === 'HU') {
        stdDev += FR_HU_ADJUST;
      } else if (data.size === 'FR') {
        stdDev -= FR_HU_ADJUST;
      }

      stdDev *= Math.sqrt(nTrials) / nTrials;

      results.trial = new NormalDist(mean, stdDev);
      results.bayesian = makeBayesian(results.prior, results.trial);

      var rbmean = results.bayesian.getMean() + rake * 0.3;
      results.rakeback = new NormalDist(rbmean, stdDev);

      $log.debug(results);
    }


    function updateHU(data) {
      if (
        !data.hasOwnProperty('speed') ||
        !data.hasOwnProperty('buyin') ||
        !data.hasOwnProperty('rake') ||
        !data.hasOwnProperty('numTourns') ||
        !data.hasOwnProperty('roi')
      ) return;

      $log.debug('updateHU', data);

      var avRoI = 100 * ((data.buyin / (data.buyin + data.rake)) - 1);
      var priorStdDev = HU_REG_RAKE;
      if (data.speed === 'Turbo') { priorStdDev = HU_TURBO_RAKE; }
      else if (data.speed === 'Hyper') { priorStdDev = HU_HYPER_RAKE; }
      results.prior = new NormalDist(avRoI, priorStdDev);

      var profitPerTourn = (data.rake + data.buyin) * data.roi;
      var pWin = (profitPerTourn + data.buyin + data.rake) / (2* data.buyin);
      var stdDev = Math.sqrt(pWin * data.numTourns * (1-pWin)) * (100/data.numTourns);
      var RoI = 100 * (profitPerTourn/(data.buyin + data.rake));

      results.trial = new NormalDist(RoI, stdDev);
      results.bayesian = makeBayesian(results.prior, results.trial);
      $log.debug(results);
    }


    function updateCustom(data) {
      if (
        !data.hasOwnProperty('rake') ||
        !data.hasOwnProperty('stdDev') ||
        !data.hasOwnProperty('trials') ||
        !data.hasOwnProperty('winrate')
      ) return;

      $log.debug('updateCustom', data);

      var prior = new NormalDist(-data.rake, data.rake);

      var stdDev = data.stdDev;
      stdDev *= Math.sqrt(data.trials) / data.trials;
      results.trial = new NormalDist(data.winrate, stdDev);

      results.bayesian = makeBayesian(results.prior, results.trial);
      $log.debug(results);
    }


    /* Class that stores and returns values from probability distributions. File also
     * includes a function that returns the Bayesian product of two distributions
     *
        function IDist() {
            this.getY = function(xValue){}
            this.getMin = function(){}
            this.getMax = function(){}
            this.getMean = function(){}
            this.getStdDev = function(){}
            this.getPxLessThan = function(xValue){}
            this.getPxMoreThan = function(xValue){}
            this.getPercentile = function(prob){}
        }
    */

    function NormalDist(mean, stdDev){
      mean = mean || 0;
      stdDev = stdDev || 1;
      var variance = Math.pow(stdDev, 2);
      this.getY = function(xValue){
        var Y;
        Y = 1/(Math.sqrt(2*variance*Math.PI))*Math.exp(-Math.pow((xValue-mean),2)/(2*variance));
        return Y;
      };
      this.getMin = function(){
        return mean - stdDev * 4;
      };
      this.getMax = function(){
        return mean + stdDev * 4;
      };
      this.getMean = function(){ return mean;};
      this.getStdDev = function(){ return stdDev;};

      this.getPxLessThan = function(xValue) {
        return integrate(this.getY, this.getMin(), xValue);
      };

      this.getPxMoreThan = function(xValue){
        return integrate(this.getY, xValue, this.getMax());
      };
    }

    function ArbDist(pointArray) {
      var mean, stdDev;

      this.getY = function(xValue){
        var iMin = 0, iMax = pointArray.length-1;
        var xMin = pointArray[iMin][0], xMax = pointArray[iMax][0];
        if ((xValue < xMin) || (xValue > xMax)) {
          return 0;
        }
        while (iMax > (iMin+1)) {
          if (xValue < ((xMin+xMax)/2)) {
            iMax = Math.ceil(iMax - ((iMax-iMin)/2));
          }
          else {
            iMin = Math.floor(iMin + ((iMax-iMin)/2));
          }
          xMin = pointArray[iMin][0];
          xMax = pointArray[iMax][0];
        }
        var diff = (xValue - pointArray[iMin][0]) / (pointArray[iMax][0] - pointArray[iMin][0]);
        return pointArray[iMin][1] + (pointArray[iMax][1] - pointArray[iMin][1]) * diff;
      };

      this.getMin = function() { return pointArray[0][0];};
      this.getMax = function() { return pointArray[pointArray.length-1][0];};
      this.getMean = function() {
        if (mean === undefined) {
          var sum = 0;
          for (var i = 0; i < pointArray.length; i++) {
            sum += pointArray[i][0] * pointArray[i][1];
          }
          sum /= 1/(pointArray[1][0] - pointArray[0][0]);
          mean = sum;
        }
        return mean;
      };
      this.getStdDev = function() {
        if (stdDev === undefined) {
          var mean = this.getMean();
          var variance = 0;
          for (var i = 0; i<pointArray.length; i++) {
            variance += Math.pow(pointArray[i][0] - mean,2) * pointArray[i][1];
          }
          variance /= 1/(pointArray[1][0] - pointArray[0][0]);
          stdDev = Math.sqrt(variance);
        }
        return stdDev;
      };
      this.getPxLessThan = function(xValue) {
        return integrate(this.getY,pointArray[0][0],xValue);
      };

      this.getPxMoreThan = function(xValue){ return 1-this.getPxLessThan(xValue); };
      this.getPercentile = function(prob, stepSize) {
        stepSize = stepSize || 0.1;

        if (prob < 0) { throw "Probability less than zero"; }
        else if (prob > 1) { throw "Probability more than one"; }
        else {
          var x = Math.floor(this.getMin()), sum = 0, a = this.getY(x), b;
          while (sum < prob) {
            x += stepSize;
            b = this.getY(x);
            sum += stepSize*(a+b)/2;
            a = b;
          }
          return x;
        }
      };
    }

    function makeBayesian(dist1, dist2){
      if (dist1 === undefined || dist2 === undefined) {
        return undefined;
      }

      var DIST_RESOLUTION = 1000, maxvalue, minvalue;

      minvalue = dist1.getMin() < dist2.getMin() ? dist1.getMin() : dist2.getMin();
      maxvalue = dist1.getMax() > dist2.getMax() ? dist1.getMax() : dist2.getMax();

      var probdist = [],
        index = 0,
        x = 0, y = 0,
        dx = (maxvalue - minvalue)/DIST_RESOLUTION;

      while (index < DIST_RESOLUTION) {
        x = minvalue + index*dx;
        y = dist1.getY(x) * dist2.getY(x);
        probdist.push([x,y]);
        index++;
      }

      var dSum = 0, b = 0, a = 0;
      for (index = 0; index < probdist.length; ++index) {
        a = probdist[index][1];
        dSum += (a + b)/2 * dx;
        b = a;
      }
      for (index = 0; index < probdist.length; ++index) {
        probdist[index][1] /= dSum;
      }

      return new ArbDist(probdist);
    }

    function integrate(f,a,b) {
      var N = 50, // precision
        h = (b-a)/N,
        diff = Math.abs(b-a),
        sum = f(a) + f(b),
        i, x;
      for (i = 1; i < N; i+=2) {
        x = a + h * i;
        sum += 4 * f(x);
      }
      for (i = 2; i < N; i+=2) {
        x = a + h * i;
        sum += 2 * f(x);
      }
      return sum * h / 3;
    }

    function getRake(game, bigblind) {
      var rake;
      if (game === 'PLO') {
        switch(bigblind*100) {
          case 2:
          case 5:
          case 10:
          case 20:
          case 25:
            rake = 18;
            break;
          case 50:
            rake = 16.5;
            break;
          case 100:
            rake = 12.5;
            break;
          case 200:
            rake = 9;
            break;
          case 400:
            rake = 6.5;
            break;
          case 500:
            rake = 4;
            break;
          default:
            rake = 3.5;
        }
      }
      else {
        switch(bigblind*100) {
          case 2:
          case 5:
          case 10:
          case 20:
          case 25:
            rake = 8.5;
            break;
          case 50:
            rake = 7.5;
            break;
          case 100:
            rake = 6.5;
            break;
          case 200:
            rake = 5.5;
            break;
          case 400:
            rake = 4.5;
            break;
          case 500:
            rake = 4;
            break;
          default:
            rake = 3.5;
        }
      }
      return rake;
    }

  });
