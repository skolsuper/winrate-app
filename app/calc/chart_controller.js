angular.module('calc')
  .controller('ChartCtrl', function ($element, $log, $scope, $window, ResultsService) {

    var chart = this;

    var container, width, height, paper, xMin, data, plots, axes, scale, key, fadeOut, fadeIn;

    chart.axesToFront = function() { axes.toFront(); return chart;};
    chart.hideKey = function() { key.hide(); };
    chart.showKey = function() { if (paper.width > 500) {key.show(); }};
    chart.getData = function(index) { return data[index]; };
    chart.getPlot = function(index) { return plots[index]; };
    chart.rbFadeOut = function() { fadeOthersOut(2); };
    chart.rbFadeIn = function() { fadeOthersIn(2); };
    chart.reDraw = reDraw;
    chart.changeData = changeData;
    chart.changeColor = changeColor;
    chart.setXMin = setXMin;

    activate();

    $window.onresize = function() {
        reDraw($element.parent().width());
    };

    $scope.$watchCollection(function () { return ResultsService;}, function (results) {
      if (!results.hasOwnProperty('bayesian')) {
        data = [0, 0, 0];
        reDraw($element.parent().width());
      } else {
        changeData(0, results.prior);
        changeData(1, results.trial);
        changeData(2, results.bayesian);
      }
    });

    function activate() {
      container = 'chart';
      width = $element.parent().width();
      height = width * 9 / 16;
      paper = new Raphael(container,width,height);
      xMin = -40;
      data = [0,0,0];
      plots = [];
      axes = drawAxes();
      scale = drawScale();
      fadeOut = new Raphael.animation({opacity: 0.2}, 500, "linear");
      fadeIn = new Raphael.animation({opacity: 1}, 500, "linear");
    }

    function reDraw(width) {
      paper.clear();
      height = width * 9 / 16;
      paper.setSize(width,height);
      axes = drawAxes();
      scale = drawScale();
      plots[0] = drawPlot(data[0],"red");
      plots[1] = drawPlot(data[1],"blue");
      plots[2] = drawPlot(data[2],"green");
      plots[0].hover(function(){fadeOthersOut(0);}, function(){fadeOthersIn(0);});
      plots[1].hover(function(){fadeOthersOut(1);}, function(){fadeOthersIn(1);});
      plots[2].hover(function(){fadeOthersOut(2);}, function(){fadeOthersIn(2);});
      drawKey();

      return chart;
    }

    function changeData(index, dist) {
      data[index] = dist;
      try {
        plots[index].animate({path: getPath(dist)}, 1500, "bounce");
          //plots[index].attr("path",getPath(dist));
      } catch (e) {
        $log.error(e);
        return reDraw();
      }
      return chart;
    }

    function changeColor(index, color) {
      plots[index].animate({fill: color, stroke: color},500);
      return chart;
    }

    function setXMin(value) {
      if (value < -40 ) { xMin = -40; }
      else if (value > -18) { xMin = -18; }
      else { xMin = value; }
      changeScale();
      return chart;
    }

    function changeScale() {
      var xZero = paper.width/2;
      var xDiff = (8/xMin) * xZero;
      var xi = [], i;
      for (i = 1; i<=scale.length/2; i++) {
        xi.push(xZero - i*xDiff);
        xi.push(xZero + i*xDiff);
      }
      for (i = 0; i < scale.length; i++) {
        scale[i].animate({x:xi[i]},1500,"bounce");
      }
    }

    function drawScale() {
      var xZero = paper.width/2;
      var xDiff = (8/xMin) * xZero;
      var scale = [];
      for (var i = 1; i <=4; i++) {
        var xi = xZero - i*xDiff;
        paper.setStart();
        var pip1 = paper.rect(xi,paper.height-20,1,5).attr({
          fill: "black"
        });
        var pipLabel1 = paper.text(xi,paper.height-7,i*8).attr({
          "font-family": "sans-serif",
          "font-size":"16px",
          "font-weight": "200",
          fill: "black"
        });
        scale.push(paper.setFinish());
        scale.push(scale[2*(i-1)].clone().attr({x:xZero + i*xDiff,text:i*-8}));
      }
      return scale;
    }

    function getPath(dist) {
      var distPath = [['M',20,paper.height-20]];
      var steps = ((paper.width-40) / 5);
      var stepSize = Math.abs(xMin * 2 / steps);
      var y=0, dy=0, i;
      if (typeof dist == 'object') {
        distPath[1] = ['L',20,paper.height-(20+(paper.height*5*dist.getY(xMin)))];
        y = paper.height*5*dist.getY(xMin);
        for (i=1; i<=steps; ++i) {
          dy = paper.height * 5 * dist.getY(xMin + (i*stepSize))-y;
          distPath.push(['l',5,-dy]);
          y += dy;
        }
      }
      else {
        for (i=0; i<=steps; ++i) {
          distPath.push(['l',5,0]);
        }
      }
      distPath.push(['L',paper.width,paper.height-20]);
      distPath.push(['Z']);
      return Raphael.path2curve(distPath);
    }

    function drawPlot(dist,color) {
      var plot = paper.path(getPath(dist)).attr({
        fill: color,
        "fill-opacity": 0.25,
        "stroke": color,
        "stroke-width": 3,
        "clip-rect": "21,21," + (paper.width-42) + "," + (paper.height - 42)
      });
      return plot;
    }

    function drawAxes() {
      var xZero = paper.width/2;
      paper.setStart();
      var xAxis = paper.rect(20,paper.height-20,paper.width-40,1).attr({
        fill: "black"
      });
      var yAxis = paper.rect(xZero,20,1,paper.height-35).attr({
        fill: "black"
      });
      var yLabel1 = paper.text(xZero-50,20,"p.d.f.(x)").attr({
        "font-family": "sans-serif",
        "font-size":"20px",
        "font-weight": "400",
        fill: "black"
      });
      var yLabel2 = paper.text(xZero,paper.height-6,"Breakeven").attr({
        "font-family": "sans-serif",
        "font-size":"16px",
        "font-weight": "200",
        fill: "black"
      });
      return paper.setFinish();
    }

    function drawKey() {
      paper.setStart();
      var keySq = [];
      var keyLabel = [];
      var labelNames = [
        "All players",
        "Winrate implied by results",
        "Bayesian inferred distribution"
        ];
      for (var i = 0; i<plots.length; i++) {
        keySq[i] = paper.rect(25,(i+1)*25,14,14).attr({
          fill: plots[i].attr("fill"),
          "fill-opacity": 0.25,
          "stroke": plots[i].attr("fill"),
          "stroke-width": 2
        });
        keyLabel[i] = paper.text(25,7+(i+1)*25,labelNames[i]).attr({
          "font-family": "sans-serif",
          "font-size":"14px",
          "font-weight": "200",
          fill: "black"
        });

        keyLabel[i].transform("t"+(keyLabel[i].getBBox().width/2+20)+",0");
      }
      return paper.setFinish();
    }

    function fadeOthersOut(index) {
      for (var i = 0; i < plots.length; i++) {
        if (i != index) { plots[i].animate(fadeOut);}
      }
      axes.toFront();
    }

    function fadeOthersIn(index) {
      for (var i = 0; i < plots.length; i++) {
        if (i != index) { plots[i].animate(fadeIn);}
      }
      axes.toFront();
    }

  });
