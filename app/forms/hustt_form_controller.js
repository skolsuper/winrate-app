angular.module('forms')
  .controller('HuSngFormCtrl', function (HuSngData) {
    var vm = this;

    vm.model = HuSngData;
    vm.options = {};
    vm.fields = [
      {
        key: 'buyin',
        type: 'input',
        templateOptions: {
          label: 'Buyin'
        }
      },
      {
        key: 'rake',
        type: 'input',
        templateOptions: {
          label: 'Rake'
        }
      },
      {
        key: 'numTourns',
        type: 'input',
        templateOptions: {
          label: 'Number of Tourns'
        }
      },
      {
        key: 'roi',
        type: 'input',
        templateOptions: {
          label: 'ROI'
        }
      }
    ];
  });
