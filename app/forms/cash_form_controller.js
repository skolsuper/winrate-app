angular.module('forms')
  .controller('CashFormCtrl', function (CashData) {
    var vm = this;

    vm.model = CashData;
    vm.options = {};
    vm.fields = [
      {
        key: 'bigblind',
        type: 'select',
        templateOptions: {
          label: 'Big blind',
          labelProp: 'value',
          options: [
            { value: 0.02 },
            { value: 0.05 },
            { value: 0.1 },
            { value: 0.2 },
            { value: 0.25 },
            { value: 0.5 },
            { value: 1 },
            { value: 2 },
            { value: 4 },
            { value: 5 },
            { value: 6 },
            { value: 10 },
            { value: 20 }
          ]
        }
      },
      {
        key: 'numHands',
        type: 'input',
        templateOptions: {
          label: 'Number of Hands'
        }
      },
      {
        key: 'winrate',
        type: 'input',
        templateOptions: {
          label: 'Winrate'
        }
      }
    ];
  });
