angular.module('forms')
  .controller('CustomFormCtrl', function (CustomData) {
    var vm = this;

    vm.model = CustomData;
    vm.options = {};
    vm.fields = [
      {
        key: 'rake',
        type: 'input',
        templateOptions: {
          label: 'Rake'
        }
      },
      {
        key: 'stdDev',
        type: 'input',
        templateOptions: {
          label: 'Std. Dev.'
        }
      },
      {
        key: 'trials',
        type: 'input',
        templateOptions: {
          label: 'Number of Trials'
        }
      },
      {
        key: 'winrate',
        type: 'input',
        templateOptions: {
          label: 'Winrate'
        }
      }
    ];
  });
