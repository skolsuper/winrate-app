angular.module('application', [
  'calc',
  'ngTouch',
  'ui.router',
  'ui.bootstrap'
])
  .config(function ($compileProvider, $logProvider, $stateProvider, $urlRouterProvider) {
    $compileProvider.debugInfoEnabled(false);
    $logProvider.debugEnabled(false);

    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('about', {
        url: '/about/',
        templateUrl: 'about/about.html'
      })
      .state('rakeback', {
        url: '/rakeback/',
        templateUrl: 'rakeback/rakeback.html'
      })
      .state('calc', {
        url: '/',
        templateUrl: 'calc/calc.html'
      });
  });
