"use strict";

var PLO_STDDEV = 140;
var NLH_STDDEV = 100;
var FR_HU_ADJUST = 15;
var HU_REG_RAKE = 5;
var HU_TURBO_RAKE = 4;
var HU_HYPER_RAKE = 2;

function dataController($scope) {

    var chart = new BayesPlot(document.getElementById('chart'),getChartWidth());

    window.onresize = function() {
        chart.reDraw(getChartWidth());
        moveRbBtn();
    };

    $scope.accordion = {
        cash: true,
        headsup: false,
        custom: false
    };
    var location = window.location.protocol + "//" + window.location.hostname + window.location.pathname;
    $scope.shareURL = location;

    $scope.showResults = false;
    $scope.winrateWord = "winrate";
    $scope.winrateSymbol = "bb/100";
    $scope.cashFormData = {
        game: "NLH",
        size: "6M",
        bigblind: "",
        numHands: "",
        winrate: "",
        profit: ""
    };
    $scope.huFormData = {
        structure: "Regular",
        buyin: "",
        rake: "",
        numTourns: "",
        roi: "",
        profit: ""
    };
    $scope.customFormData = {
        rake: "",
        stdDev: "",
        numTrials: "",
        winrate: ""
    };

    angular.element(document).ready(pullFormInputFromURL);

    $scope.results = {
        probably1: 0,
        probably2: 0,
        definitely1: 0,
        definitely2: 0,
        resultError: 0,
        percentChance: 0,
        winrateResult: 0
    };
    $scope.rbResults = {
        probably1: 0,
        probably2: 0,
        definitely1: 0,
        definitely2: 0,
        resultError: 0,
        percentChance: 0,
        winrateResult: 0
    };

    $scope.bayesianWinrate = function() {
        var priorMean, priorStdDev, trialMean, trialStdDev;
        if ($scope.accordion.cash) {
            $scope.shareURL = location + "?form=cash&data=" + encodeURIComponent(JSON.stringify($scope.cashFormData));
            $scope.winrateWord = "winrate";
            $scope.winrateSymbol = "bb/100";

            if ($scope.cashFormData.numHands === "" || $scope.cashFormData.winrate === "" || $scope.cashFormData.bigblind === "") {
                //Return early if any fields are empty
                reset();
                return $scope.results.winrateResult;
            }

            priorMean = -getRake($scope.cashFormData.bigblind,$scope.cashFormData.game);
            priorStdDev = -priorMean;

            if ($scope.cashFormData.game == "PLO") { trialStdDev = PLO_STDDEV; }
            else { trialStdDev = NLH_STDDEV; }
            if ($scope.cashFormData.size == "HU") { trialStdDev += FR_HU_ADJUST; }
            if ($scope.cashFormData.size == "FR") { trialStdDev -= FR_HU_ADJUST; }

            var nTrials = $scope.cashFormData.numHands*0.01;

            trialMean = parseFloat($scope.cashFormData.winrate);
            $scope.cashFormData.profit = trialMean * nTrials * $scope.cashFormData.bigblind;
            trialStdDev *= Math.sqrt(nTrials) / nTrials;
        }

        else if ($scope.accordion.headsup) {
            $scope.shareURL = location + "?form=headsup&data=" + encodeURIComponent(JSON.stringify($scope.huFormData));
            $scope.winrateWord = "ROI";
            $scope.winrateSymbol = "%";

            if ($scope.huFormData.buyin === "" || $scope.huFormData.rake === "" || $scope.huFormData.numTourns === "" || $scope.huFormData.roi === "") {
                reset();
                return $scope.results.winrateResult;
            }

            var totalBuyin = parseFloat($scope.huFormData.buyin)+parseFloat($scope.huFormData.rake);
            priorMean = (($scope.huFormData.buyin/(totalBuyin))-1)*100;
            if ($scope.huFormData.structure == "Regular") { priorStdDev = 5;}
            else if ($scope.huFormData.structure == "Turbo") { priorStdDev = 4;}
            else { priorStdDev = 2;}

            trialMean = parseFloat($scope.huFormData.roi);
            var pWin = ((($scope.huFormData.roi/100)+1) * totalBuyin) / (2* $scope.huFormData.buyin);
            var numTourns = parseInt($scope.huFormData.numTourns);
            trialStdDev = Math.sqrt(pWin * numTourns * (1-pWin)) * (100/numTourns);

            $scope.huFormData.profit = ($scope.huFormData.roi/100) * totalBuyin * numTourns;
        }

        else if ($scope.accordion.custom) {
            $scope.shareURL = location + "?form=custom&data=" + encodeURIComponent(JSON.stringify($scope.customFormData));
            $scope.winrateWord = "winrate";
            $scope.winrateSymbol = " (bb/100 | BB/100 | %)";

            if ($scope.customFormData.rake === "" || $scope.huFormData.stdDev === "" || $scope.huFormData.numTrials === "" || $scope.huFormData.winrate === "") {
                reset();
                return $scope.results.winrateResult;
            }

            priorMean = -$scope.customFormData.rake;
            priorStdDev = -priorMean;

            trialMean = $scope.customFormData.winrate;
            trialStdDev = $scope.customFormData.stdDev;
            var nTrials = $scope.customFormData.numTrials;
            trialStdDev *= Math.sqrt(nTrials) / nTrials;
        }

        else {
            reset();
            return $scope.results.winrateResult;
        }

        var prior = new NormalDist(priorMean,priorStdDev);
        var trial = new NormalDist(trialMean,trialStdDev);
        var bayesian = makeBayesian(prior,trial);

        $scope.results = {
            probably1: bayesian.getPercentile(0.15).toFixed(1),
            probably2: bayesian.getPercentile(0.85).toFixed(1),
            definitely1: bayesian.getPercentile(0.025).toFixed(1),
            definitely2: bayesian.getPercentile(0.975).toFixed(1),
            resultError: bayesian.getStdDev().toFixed(1),
            percentChance: (100 * bayesian.getPxLessThan(0)).toFixed(0),
            winrateResult: bayesian.getMean().toFixed(1)
        };

        var rakeback = new NormalDist(bayesian.getMean() + 0.3*-priorMean,bayesian.getStdDev())
        $scope.rbResults = {
            probably1: (rakeback.getMean()-rakeback.getStdDev()).toFixed(1),
            probably2: (rakeback.getMean()+rakeback.getStdDev()).toFixed(1),
            definitely1: (rakeback.getMean()-2*rakeback.getStdDev()).toFixed(1),
            definitely2: (rakeback.getMean()+2*rakeback.getStdDev()).toFixed(1),
            resultError: rakeback.getStdDev().toFixed(1),
            percentChance: (100 * rakeback.getPxLessThan(0)).toFixed(0),
            winrateResult: rakeback.getMean().toFixed(1)
        };

        chart.setXMin(prior.getMin())
            .changeData(0,prior)
            .changeData(1,trial)
            .changeData(2,bayesian)
            .axesToFront()
            .showKey();

        $scope.showResults = true;
        moveRbBtn();

        return $scope.results.winrateResult;
    };

    function reset() {
        for (var i = 0; i < 3; i++) {
            chart.changeData(i,0);
        }
        chart.hideKey();
        $scope.showResults = false;
        $scope.shareURL = location;
    }

    function getRake(bigblind,game) {
        var rake;
        if (game == "PLO") {
            switch(parseInt(bigblind*100)) {
                case 2:
                case 5:
                case 10:
                case 20:
                case 25:
                    rake = 18;
                    break;
                case 50:
                    rake = 16.5;
                    break;
                case 100:
                    rake = 12.5;
                    break;
                case 200:
                    rake = 9;
                    break;
                case 400:
                    rake = 6.5;
                    break;
                case 500:
                    rake = 4;
                    break;
                default:
                    rake = 3.5;
            }
        }
        else {
            switch(parseInt(bigblind*100)) {
                case 2:
                case 5:
                case 10:
                case 20:
                case 25:
                    rake = 8.5;
                    break;
                case 50:
                    rake = 7.5;
                    break;
                case 100:
                    rake = 6.5;
                    break;
                case 200:
                    rake = 5.5;
                    break;
                case 400:
                    rake = 4.5;
                    break;
                case 500:
                    rake = 4;
                    break;
                default:
                    rake = 3.5;
            }
        }
        return rake;
    }

    function getChartWidth() {
        return (document.getElementById('chart').parentNode.clientWidth < 800) ? document.getElementById('chart').parentNode.clientWidth : 800;
    }

    function moveRbBtn() {
        var rbButton = document.getElementById('getRakeback');
        if (rbButton.parentNode.clientWidth > 690) {
            rbButton.style.position = "absolute";
            rbButton.style.display = "inline";
            rbButton.style.top = "75px";
            rbButton.style.left = "500px";
        }
        else {
            rbButton.style.position = "relative";
            rbButton.style.display = "block";
            rbButton.style.top = "0px";
            rbButton.style.left = "0px";
        }
    }
    function pullFormInputFromURL() {
        var urlParams = window.location.search.substring(1).split('&');
        if (urlParams === "") { return; }
        $scope.accordion = {
            cash: false,
            headsup: false,
            custom: false
        };
        var form = urlParams[0].split('=')[1];
        var data = JSON.parse(decodeURIComponent(urlParams[1].split('=')[1]));
        if (form === "cash") {
            $scope.accordion.cash = true;
            $scope.cashFormData = data;
        }
        else if (form === "headsup") {
            $scope.accordion.headsup = true;
            $scope.huFormData = data;
        }
        else if (form === "custom") {
            $scope.accordion.custom = true;
            $scope.customFormData = data;
        }
        $scope.bayesianWinrate();
        $scope.$apply();
    }
}

dataController.$inject = ['$scope']; //Needed for simple optimization via closure compiler
