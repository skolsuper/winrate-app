/* Sets up the page and takes care of js presentation bits
 * such as the sliders and mouseovers and resizing the graph. Coordinates
 * taking input from the user and presenting it in the graph and results
 * panes.
 */

var PLO_STDDEV = 140
var NLH_STDDEV = 100
var FR_HU_ADJUST = 15
var HU_REG_RAKE = 5
var HU_TURBO_RAKE = 4
var HU_HYPER_RAKE = 2

$(document).ready(function(){

    $(function() {
		$( "#accordion" ).accordion({heightStyle: "content" });
	});

	$(function() {
		$('#winrate').slider({
			orientation: "horizontal",
			max: 30,
			min: -10,
			step: 0.1,
			value: 0,
			slide: function(){$('#winrateText').val($('#winrate').slider("value"));},
			change: updateHandler
		});
		$('#numHands').slider({
			orientation: "horizontal",
			max: 300000,
			min: 5000,
			step: 5000,
			value: 10000,
			slide: function(){$('#numHandsText').val($('#numHands').slider("value"));},
			change: updateHandler
		});
		$('#buyin').slider({
			orientation: "horizontal",
			max: 300,
			min: 0,
			step: 1,
			value: 50,
			slide: function(){
				$('#buyinText').val($('#buyin').slider("value"));
				},
			change: function(){
				$('#rake').slider("option", "max", $('#buyin').slider("value")/8);
				$('#rake').slider("option", "value", $('#buyin').slider("value")/20);
				$('#huProfit').slider("option", "max", $('#buyin').slider("value")*100);
				$('#huProfit').slider("option", "min", $('#buyin').slider("value")*-30);
				$('#huProfit').slider("option", "step", $('#buyin').slider("value")/10);
				updateHandler();}
		});
		$('#rake').slider({
			orientation: "horizontal",
			max: 10,
			min: 0,
			step: 0.1,
			value: 2.5,
			slide: function(){$('#rakeText').val($('#rake').slider("value"));},
			change: updateHandler
		});
		$('#huProfit').slider({
			orientation: "horizontal",
			max: 5000,
			min: -1000,
			step: 10,
			value: 0,
			slide: function(){$('#huProfitText').val($('#huProfit').slider("value"));},
			change: updateHandler
		});
		$('#numTourns').slider({
			orientation: "horizontal",
			max: 1000,
			min: 10,
			step: 10,
			value: 50,
			slide: function(){$('#numTournsText').val($('#numTourns').slider("value"));},
			change: updateHandler
		});
		$('#customRake').slider({
			orientation: "horizontal",
			max: 30,
			min: 0,
			step: 0.1,
			value: 8,
			slide: function(){$('#customRakeText').val($('#customRake').slider("value"));},
			change: updateHandler
		});
		$('#customStdDev').slider({
			orientation: "horizontal",
			max: 250,
			min: 20,
			step: 1,
			value: 100,
			slide: function(){$('#stdDevText').val($('#customStdDev').slider("value"));},
			change: updateHandler
		});
		$('#customHands').slider({
			orientation: "horizontal",
			max: 300000,
			min: 5000,
			step: 5000,
			value: 10000,
			slide: function(){$('#customHandsText').val($('#customHands').slider("value"));},
			change: updateHandler
		});
		$('#customProfit').slider({
			orientation: "horizontal",
			max: 300,
			min: -30,
			step: 1,
			value: 0,
			slide: function(){$('#customProfitText').val($('#customProfit').slider("value"));},
			change: updateHandler
		});

        //Putting this here makes sure sliders are initialized before trying to assign values from url
		if (window.location.search.substring(1) !== "") {
            pullFormInputFromURL();
		}
	});

    $('#selectShareURL').click(function() {
        document.getElementById("shareURL").select();
    });

	var chart = new BayesPlot(document.getElementById('chart'),getChartWidth());

	$('#chart').hover(chart.showKey, chart.hideKey);

	$(window).resize(function(){
		chart.reDraw(getChartWidth());
		moveRbBtn();
	});

	$('.headsupinput').change(function(){
		$(this).parent().next().slider("option","value",$(this).val());
		updateHandler('headsup');
	});
	$('.cashinput').change(function(){
		$(this).parent().next().slider("option","value",$(this).val());
		updateHandler('cash');
	});
	$('.custominput').change(function(){
		$(this).parent().next().slider("option","value",$(this).val());
		updateHandler('custom');
	});

	$('#bigblind').change(updateHandler);

	$('.cashbtnrow1 label').click(function() {
		$('.cashbtnrow1 label').removeClass("active");
		$(this).addClass("active");
		updateHandler('cash');
	});
	$('.cashbtnrow2 label').click(function() {
		$('.cashbtnrow2 label').removeClass("active");
		$(this).addClass("active");
		updateHandler('cash');
	});
	$('.hubtn label').click(function() {
		$('.hubtn label').removeClass("active");
		$(this).addClass("active");
		if ($(this).attr("id") === "huOptionReg") {
			$('#rake').slider("option", "value", $('#buyin').slider("value")*(HU_REG_RAKE/100));
		}
		else if ($(this).attr("id") === "huOptionTurbo") {
			$('#rake').slider("option", "value", $('#buyin').slider("value")*(HU_TURBO_RAKE/100));
		}
		else { $('#rake').slider("option", "value", $('#buyin').slider("value")*(HU_HYPER_RAKE/100));}
		updateHandler('headsup');
	});

	$(function() {
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "left top",
				at: "right+5 top-5"
			}
		});
	});
	$('.details').hide();
    $('#share').hide();

	function getChartWidth() {
		return ($('#chart').parent().width() < 800) ? $('#chart').parent().width() : 800;
	}

	function updateHandler(game) {
		//Uses this->hasClass to identify slider callbacks that don't specify game param
		if ($(this).hasClass('headsup') || game == 'headsup') {
			updateTextBoxes("headsup");
      var buyin = parseFloat(document.getElementById('buyinText').value), rake = parseFloat(document.getElementById('rakeText').value);
      var avRoI = 100*((buyin/(buyin+rake))-1);
      var priorStdDev = HU_REG_RAKE;
      if (document.getElementById('huOptionTurbo').className.search('active') != -1) { priorStdDev = HU_TURBO_RAKE; }
      else if (document.getElementById('huOptionHyper').className.search('active') != -1) { priorStdDev = HU_HYPER_RAKE; }
      var prior = new NormalDist(avRoI,priorStdDev);

      var numTourns = parseInt(document.getElementById('numTournsText').value);
      var profit = parseFloat(document.getElementById('huProfitText').value);
      var profitPerTourn = profit/numTourns;
      var pWin = (profitPerTourn + buyin + rake) / (2* buyin);
      var stdDev = Math.sqrt(pWin * numTourns * (1-pWin)) * (100/numTourns);
      var RoI = 100 * (profitPerTourn/(buyin+rake));

      var trial = new NormalDist(RoI,stdDev);

      var formInput = {
          game: "headsup",
          prior: prior,
          trial: trial
      };
		}
		else if ($(this).hasClass('custom') || game == 'custom') {
			updateTextBoxes("custom");
			var rake = document.getElementById('customRakeText').value;
      var prior = new NormalDist(-rake,rake);

      var stdDev = document.getElementById('stdDevText').value;
      var nTrials = document.getElementById('customHandsText').value * 0.01;
      stdDev *= Math.sqrt(nTrials) / nTrials;
      var mean = document.getElementById('customProfitText').value * 100 / nTrials;
      var trial = new NormalDist(mean,stdDev);

      var formInput = {
          game: "custom",
          prior: prior,
          trial: trial
      };
		}
		else if ($(this).hasClass('cash') || game == 'cash') {
			updateTextBoxes("cash");
			var bigblind = parseFloat(document.getElementById('bigblind').value);
            var rake = getRake(bigblind);
            var prior = new NormalDist(-rake,rake);

            var stdDev;
            if (document.getElementById('cashOptionPLO').className.search('active') != -1) { stdDev = PLO_STDDEV; }
            else { stdDev = NLH_STDDEV; }
            if (document.getElementById('cashOptionHU').className.search('active') != -1) { stdDev += FR_HU_ADJUST; }
            if (document.getElementById('cashOptionFR').className.search('active') != -1) { stdDev -= FR_HU_ADJUST; }

            var bbWinnings = document.getElementById('cashProfit').value / bigblind;
            var nTrials = document.getElementById('numHandsText').value*0.01;
            var mean = bbWinnings / nTrials;
            if (isNaN(mean)) {
                mean = document.getElementById('winrateText').value;
            }
            stdDev *= Math.sqrt(nTrials) / nTrials;
            var trial = new NormalDist(mean,stdDev);
            var formInput = {
                game: "cash",
                prior: prior,
                trial: trial
            };
		}
		else { return; }
		updateResults(formInput);
		$('.details').slideDown();
		moveRbBtn();
	}

	function updateResults(formInput) {
		var bayesian = makeBayesian(formInput.prior,formInput.trial);
		chart.setXMin(formInput.prior.getMin());
		chart.changeData(0,formInput.prior);
		chart.changeData(1,formInput.trial);
		chart.changeData(2,bayesian);
		chart.axesToFront();

		var winrateword, symbol;
		if (formInput.game == "headsup") { winrateword = "ROI"; symbol = "%"; }
		else /*if (formInput.game ==" cash")*/ { winrateword = "winrate"; symbol = "bb/100"; }
		//else { winrateword = "winrate"; symbol = ""; }

		$('.winrateword').text(winrateword);
        $('.symbol').text(symbol);
		var mean = bayesian.getMean(), stdDev = bayesian.getStdDev();
		$('#winrateResult').html(mean.toFixed(1).toString() + "&plusmn" +
		 (bayesian.getStdDev()).toFixed(1).toString() + symbol);
		var percentChance = (100*(bayesian.getPxLessThan(0))).toFixed(0);
		if (percentChance < 1) { percentChance = '<1';}
		$('#percentChance').text(percentChance);
		$('#probably1').text(bayesian.getPercentile(0.15).toFixed(1).toString());
		$('#probably2').text(bayesian.getPercentile(0.85).toFixed(1).toString());
		$('#definitely1').text(bayesian.getPercentile(0.025).toFixed(1).toString());
		$('#definitely2').text(bayesian.getPercentile(0.975).toFixed(1).toString());

		var rbmean = mean - formInput.prior.getMean()*0.3;
		var rakeback = new NormalDist(rbmean,stdDev);

		$('#rbWinrateResult').html(rbmean.toFixed(1).toString() + "&plusmn" +
		 (rakeback.getStdDev()).toFixed(1).toString() + symbol);
		var rbPercentChance = (100*(rakeback.getPxLessThan(0))).toFixed(0);
		if (rbPercentChance < 1) { rbPercentChance = '<1';}
		$('#rbPercentChance').text(rbPercentChance);
		$('#rbProbably1').text((rbmean-stdDev).toFixed(1).toString());
		$('#rbProbably2').text((rbmean+stdDev).toFixed(1).toString());
		$('#rbDefinitely1').text((rbmean-stdDev*2).toFixed(1).toString());
		$('#rbDefinitely2').text((rbmean+stdDev*2).toFixed(1).toString());

		$('#rakeback').hover(function(){
			chart.changeData(2,rakeback);
			chart.rbFadeOut()
		},function(){
			chart.rbFadeIn();
			chart.changeData(2,bayesian);
		});
	}

	function updateTextBoxes(game) {
        var strShareURL = "http://www.whatsmywinrate.com/index.html?tab=";
		if (game == "cash") {
            strShareURL += "0&";
			var winrate = $('#winrate').slider("value");
			var numHands = $('#numHands').slider("value");
			var profit = parseFloat($('#bigblind').val()) * winrate * numHands/100;
			$('#winrateText').val(winrate);
			$('#numHandsText').val(numHands);
			$('#cashProfit').val(profit.toFixed(2))

            $('.cashbtnrow1 label').each(function() {
                if ($(this).hasClass('active')) {
                    strShareURL += "cashbtnrow1=" + $(this).attr('id') + "&";
                }
            });
            $('.cashbtnrow2 label').each(function() {
                if ($(this).hasClass('active')) {
                    strShareURL += "cashbtnrow2=" + $(this).attr('id') + "&";
                }
            });
            strShareURL += "bigblind=" + $('#bigblind').val();
            strShareURL += "&numHands=" + numHands;
            strShareURL += "&winrate=" + winrate;
		}
		else if (game == "headsup") {
			strShareURL += "1&";
            $('#buyinText').val($('#buyin').slider("value"));
			$('#rakeText').val($('#rake').slider("value"));
			$('#huProfitText').val($('#huProfit').slider("value"));
			$('#numTournsText').val($('#numTourns').slider("value"));
            $('.hubtn label').each(function() {
                if ($(this).hasClass('active')) {
                    strShareURL += "hubtn=" + $(this).attr('id') + "&";
                }
            });
            strShareURL += "buyin=" + $('#buyin').slider("value");
            strShareURL += "&rake=" + $('#rake').slider("value");
            strShareURL += "&huProfit=" + $('#huProfit').slider("value");
            strShareURL += "&numTourns=" + $('#numTourns').slider("value");
		}
		else {
            strShareURL += "2&";
			$('#customRakeText').val($('#customRake').slider("value"));
			$('#stdDevText').val($('#customStdDev').slider("value"));
			$('#customHandsText').val($('#customHands').slider("value"));
			$('#customProfitText').val($('#customProfit').slider("value"));
            strShareURL += "customRake=" + $('#customRake').slider("value");
            strShareURL += "&customStdDev=" + $('#customStdDev').slider("value");
            strShareURL += "&customHands=" + $('#customHands').slider("value");
            strShareURL += "&customProfit=" + $('#customProfit').slider("value");
		}
        $('#shareURL').val(strShareURL);
        $('#share').slideDown();
	}

	function moveRbBtn() {
		if ($('#rakeback').width() < 690) {
			$('#rakeback a').css({
				"position": "relative",
			    "display": "block",
			    "top": "0px",
			    "left": "0px"
			});
		}
		else {
			$('#rakeback a').css({
				"position": "absolute",
				"display" : "inline",
			    "top": "75px",
			    "left": "500px"
			});
		}
	}

    function pullFormInputFromURL() {
        var urlParams = window.location.search.substring(1).split('&');
        for (var i=1; i < urlParams.length; i++) {
            var urlParam = urlParams[i].split('=');
            var paramSelector = '#' + urlParam[0];

            try {
                $(paramSelector).slider("option","value",urlParam[1]);
                if (paramSelector == "#cashbtnrow1") {
                    $('.cashbtnrow1 label').removeClass("active");
                    var btnId = "#" + urlParam[1];
                    $(btnId).addClass("active");
                }
                else if (paramSelector == "#cashbtnrow2") {
                    $('.cashbtnrow2 label').removeClass("active");
                    var btnId = "#" + urlParam[1];
                    $(btnId).addClass("active");
                }
                else if (paramSelector == "#hubtn") {
                    $('.hubtn label').removeClass("active");
                    var btnId = "#" + urlParam[1];
                    $(btnId).addClass("active");
                }
            }
            catch(err) {
                if (paramSelector == "#bigblind") {
                    $(paramSelector).val(urlParam[1]);
                }
            }
        }
        var tab = parseInt(urlParams[0].split('=')[1])
        $('#accordion').accordion("option","active", tab);
    }
    function getRake(bigblind) {
        var rake;
        if (document.getElementById('cashOptionPLO').className.search('active') != -1) {
            switch(bigblind*100) {
                case 2:
                case 5:
                case 10:
                case 20:
                case 25:
                    rake = 18;
                    break;
                case 50:
                    rake = 16.5;
                    break;
                case 100:
                    rake = 12.5;
                    break;
                case 200:
                    rake = 9;
                    break;
                case 400:
                    rake = 6.5;
                    break;
                case 500:
                    rake = 4;
                    break;
                default:
                    rake = 3.5;
            }
        }
        else {
            switch(bigblind*100) {
                case 2:
                case 5:
                case 10:
                case 20:
                case 25:
                    rake = 8.5;
                    break;
                case 50:
                    rake = 7.5;
                    break;
                case 100:
                    rake = 6.5;
                    break;
                case 200:
                    rake = 5.5;
                    break;
                case 400:
                    rake = 4.5;
                    break;
                case 500:
                    rake = 4;
                    break;
                default:
                    rake = 3.5;
            }
        }
        return rake;
    }
});
