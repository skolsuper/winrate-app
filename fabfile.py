from fabric.api import *
from fabric.context_managers import warn_only
from fabric.contrib.files import append, contains, exists


env.key_filename = '/home/james/.ssh/id_rsa'
env.hosts = ['root@45.63.16.71']

APP_REPO = 'https://skolsuper@bitbucket.org/skolsuper/winrate-app.git'
APP_NAME = 'whatsmywinrate'


def update():
    local('grunt build')
    with cd('/webapps'):
        create_or_pass([APP_NAME])
        put('dist/*.*', APP_NAME)

    with cd('/etc/nginx/sites-enabled'):
        put(APP_NAME + '.nginx.conf', '/etc/nginx/sites-enabled/')
    sudo('nginx -t')
    sudo('service nginx restart')


def deploy():
    local('grunt build')
    update_os()

    sudo('apt-get install -y nginx')
    if not exists('/webapps'):
        sudo('mkdir /webapps')
    with cd('/webapps'):
        create_or_pass([APP_NAME])
        put('dist/*.*', APP_NAME)

    with cd('/etc/nginx/sites-enabled'):
        if exists('default'):
            sudo('rm default')
        put(APP_NAME + '.nginx.conf', '/etc/nginx/sites-enabled/')
    sudo('nginx -t')
    sudo('service nginx restart')


def create_or_pass(dir_list):
    for dirname in dir_list:
        if not exists(dirname, use_sudo=True):
            sudo('mkdir {}'.format(dirname))

def update_os():
    with hide('stdout'):
        sudo('apt-get update')
        sudo('apt-get upgrade -y')
