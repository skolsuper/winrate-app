'use strict';

module.exports = function (grunt) {

  require('jit-grunt')(grunt, {
    scsslint: 'grunt-scss-lint',
    ngtemplates: 'grunt-angular-templates',
    json_server: 'grunt-json-server',
    replace: 'grunt-text-replace',
  });

  grunt.initConfig({

    appConfig: {
      app: {
        js: ['./app/**/*.js', '!./app/js/*.js'],
        css: './app/main.css',
        templates: './app/**/*.html'
      },
      dist: {
        css: './dist/main.css',
        js: './dist/app.js',
      }
    },

    clean: {
      serve: ['.tmp'],
      build: ['.tmp', 'dist']
    },

    copy: {
      serve: {
        files: [
          {src: '<%= appConfig.app.css %>', dest: '.tmp/main.css' },
          {src: 'app/index.html', dest: '.tmp/index.html' }
        ]
      },
      build: {
        src: '<%= appConfig.app.css %>',
        dest: './dist/main.css'
      }
    },

    concat: {
      app: {
        src: ['<%= appConfig.app.js %>', '<%= ngtemplates.all.dest %>'],
        filter: function (path) { return !/.*test\.js$/.test(path); },
        dest: '.tmp/app.js',
        options: {
          banner: ';\n\n(function(angular, undefined) {\n\n\'use strict\';\n\n',
          footer: '})(angular);'
        },
      }
    },

    concurrent: {
      serve: {
        tasks: ['watch', 'connect:server'],
        options: {
          logConcurrentOutput: true
        }
      }
    },

    connect: {
      server: {
        options: {
          port: 9001,
          open: true,
          base: ['.tmp', 'mocks', 'bower_components'],
          livereload: true,
          keepalive: true
        }
      }
    },

    jshint: {
      options: {
        jshintrc: true,
        reporter: require('jshint-stylish'),
        globals: { angular: false }
      },
      app: ['<%= appConfig.app.js %>', 'mocks/*.js']
    },

    ngAnnotate: {
      all: {
        files: [{
          '<%= concat.app.dest %>': '<%= concat.app.dest %>',
          '.tmp/mock-app.js': 'mocks/mock-app.js'
        }]
      }
    },

    ngtemplates:  {
      all: {
        cwd: './app',
        src: ['**/*.html', '!index.html'],
        dest: '.tmp/templates.js',
        options: {
          module: 'application',
          htmlmin: {
            collapseBooleanAttributes:      true,
            collapseWhitespace:             true,
            removeAttributeQuotes:          true,
            removeComments:                 true,
            removeEmptyAttributes:          true,
            removeRedundantAttributes:      false,
            removeScriptTypeAttributes:     true,
            removeStyleLinkTypeAttributes:  true
          }
        }
      }
    },

    replace: {
      build: {
        src: './app/index.html',
        dest: './dist/index.html',
        replacements: [
          {
            from: 'mockApp',
            to: 'application'
          },
          {
            from: /^.+mock-.+$/gm,
            to: ''
          },
          {
            from: /<!-- bower:js -->(.|\n)*<!-- endbower -->/m,
            to: ''
          },
          {
            from: /^.*cdn-scripts.*$/gm,
            to: ''
          },
          {
            from: /<!-- bower:css -->(.|\n)*<!-- endbower -->/m,
            to: '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css" />'
          }
        ]
      }
    },

    uglify: {
      all: {
        files: {
          '<%= appConfig.dist.js %>': '<%= concat.app.dest %>'
        }
      }
    },

    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        files: ['<%= appConfig.app.js %>', './mocks/*.js', '!./mocks/index.js'],
        tasks: ['jshint', 'concat', 'ngAnnotate']
      },
      css: {
        files: ['<%= appConfig.app.css %>'],
        tasks: ['copy:serve']
      },
      index: {
        files: ['app/index.html'],
        tasks: ['copy:html']
      },
      templates: {
        files: ['<%= appConfig.app.templates %>'],
        tasks: ['ngtemplates', 'concat', 'ngAnnotate']
      },
      livereload: {
        options: {
          livereload: '<%= connect.server.options.livereload %>'
        },
        files: [
          '.tmp/*',
          '<%= ngtemplates.all.dest %>'
        ]
      },
      gruntfile: {
        files: ['Gruntfile.js'],
      }
    },

    wiredep: {
      options: {
        devDependencies: true
      },
      dist: {
        src: 'Gruntfile.js',
        fileTypes : {
          js: {
            block: /(([\s\t]*)\/{2}\s*?bower:\s*?(\S*))(\n|\r|.)*?(\/{2}\s*endbower)/gi,
            detect: {
              js: /'(.*\.js)'/gi
            },
            replace: {
              js: '\'{{filePath}}\','
            }
          }
        },
        options: {
          devDependencies: false
        }
      },
      html: {
        src: 'app/index.html',
        options: {
          ignorePath: '../bower_components',
          overrides: {
            bootstrap: {
              main: 'dist/css/bootstrap.min.css'
            },
            'angular-ui-bootstrap': {
              main: 'ui-bootstrap.min.js'
            }
          }
        }
      }
    },

  });

  grunt.event.on('watch', function (action, filepath, target) {
    if (target === 'json_db') {
      grunt.log.writeln(filepath + ' ' + action);
    }
  });

  grunt.registerTask('serve', [
    'jshint',
    'clean:serve',
    'wiredep',
    'ngtemplates',
    'concat:app',
    'ngAnnotate',
    'copy:serve',
    'concurrent:serve'
  ]);

  grunt.registerTask('test', [
    'jshint',
    'clean:serve',
    'wiredep',
    'ngtemplates',
    'concat:app',
    'ngAnnotate',
    'clean:serve'
  ]);

  grunt.registerTask('build', [
    'jshint',
    'clean:build',
    'wiredep',
    'replace',
    'ngtemplates',
    'concat',
    'ngAnnotate',
    'uglify',
    'copy:dist',
    'clean:serve'
  ]);

};
